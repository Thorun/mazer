#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <vector>
#include <cstdlib>
class Map
{
    public:
        /** Default constructor
        * \param size the size of map (it will be a size*size matrix)
         */
        Map(int size);
        /** Default destructor */
        virtual ~Map();
        /** Access m_size
         * \return The current value of m_size
         */
        int Getsize() { return m_size; }

/** \brief return the value of a cell at position x,y
 *
 * \param x the position of the cell on x axis
 * \param y the position of the cell on y axis
 * \return int the value of the cell at x,y
 *
 */
        int getVal(int x, int y)
        {
            return m_matrix.at(x+(y*m_size));
        }

/** \brief set the value of a cell at position x,y
 *
 * \param x the position of the cell on x axis
 * \param y the position of the cell on y axis
 * \param val the value of the cell at position x,y
 *
 */
        void setVal(int x, int y,int val)
        {
            m_matrix[(x+(y*m_size))] = val;
        }


        bool coinFlip();//!< randomly return true or false with aprox. 50% chance.

        void randomap();//!< make a crap random map

        void display();//!< display the map on consol (display integer)

        void displayHR();//!< display the map on consol (with blank for 0, # for 1, just look inside)

        void border();//!< set the border of the matrix to 1.

    private:
        int m_size; //!< size of the map (matrix is m_size*m_size)
        std::vector<int> m_matrix; //!< Member variable "m_matrix"
};

#endif // MAP_H
