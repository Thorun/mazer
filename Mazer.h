#ifndef MAZER_H
#define MAZER_H
#include "Map.h"
#include <cstdlib>
#include <deque>

/** \brief this is a small random function that return a pseudo random integer between a and b (considering a<b)
 *
 * \param a low limit
 * \param b high limit
 * \return int random between a and b
 *
 */
int random(int a, int b);

/** \brief PoD class to contain coordinates
 */
class coord
{
public :
    coord()
    {
        x = 0;
        y =0;
    }
    coord(int i, int j)
    {
        x = i;
        y=j;
    }
    int x;
    int y;
};


/** \brief This class is a maze generator
 */
class Mazer
{
public:

    /** \brief Constructor of the class, must link to a map
     * \param m is a pointer toward a Map
     */
    Mazer(Map* m);

    /** Default destructor */
    virtual ~Mazer();

    /** \brief This is the main function that will launch the maze generation.
     *
     * \param TL coordinate of the Top Left corner of the zone
     * \param TR coordinate of the Top RIGHT corner of the zone
     * \param BL coordinate of the Bottom Left corner of the zone
     * \param BR coordinate of the Bottom RIGHT corner of the zone
     */
    void Maze(coord TL,coord TR,coord BL, coord BR);

private:

    Map* m_map;//!< this is the pointer toward the map


    /** \brief MakeWallVertical is a function that will add a vertical wall on the x coordinate
    *
    * \param x integer coordinate x on which the wall will be added
    * \param starty integer coordinate y on which the wall will start
    * \param endy integer coordinate y on which the wall will end (this imply starty<endy)
    *
    */
    void MakeWallVertical(int x,int starty, int endy);

    /** \brief MakeWallHorizontal is a function that will add a vertical wall on the x coordinate
    *
    * \param y integer coordinate y on which the wall will be added
    * \param startx integer coordinate x on which the wall will start
    * \param endx integer coordinate x on which the wall will end (this imply startx<endx)
    *
    */
    void MakeWallHorizontal(int y,int startx, int endx);
};

#endif // MAZER_H
