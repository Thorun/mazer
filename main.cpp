/*
Copyright (C) 2014  Guislain M.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */

#include <iostream>
#include "Map.h"
using namespace std;
#include <ctime>
#include <cstdlib>
#include "Mazer.h"
#include "AStar.h"
int main()
{
    srand (time(NULL));
    cout << endl;

    const int SIZE = 64;
    Map m(SIZE);
    Mazer mm(&m);
    //m.randomap();

    m.border();

    mm.Maze(coord(0,0),coord(SIZE-1,0),coord(0,SIZE-1),coord(SIZE-1,SIZE-1));

    AStar pf(&m);
    int sx = random(1,SIZE-1);
    int sy = random(1,SIZE-1);
    while(m.getVal(sx,sy))
    {
        sx = random(1,SIZE-1);
        sy = random(1,SIZE-1);
    }

    int ex = random(1,SIZE-1);
    int ey = random(1,SIZE-1);

    while(m.getVal(ex,ey))
    {
        ex = random(1,SIZE-1);
        ey = random(1,SIZE-1);
    }

  m.setVal(sx,sy,3);
    m.setVal(ex,ey,3);
    m.displayHR();
    pf.calculatePath(sx,sy,ex,ey);
    m.displayHR();
    return 0;
}
