#ifndef ASTAR_H
#define ASTAR_H

#include "Map.h"
#include <deque>
#include <algorithm>


const int INFINITY = 99999999;//!< yes i'm kind of a lazy person

float heuristicFunction(int cx,int cy, int gx, int gy);//!< function to calculate the heuristic value of the cells

/** \brief State this class is a cell containing the information needed to perform the pathfinding
 */
class State
{
public:

    State(int i, int j,int dif){x = i; y = j;H = 0; G = 0; D=dif; F = 0; parent = NULL; inOpenList = false;inClosedList = false;}

    int H;//!< heuristic value
    int G;//!< movement cost

    int D;//!< difficulty of this state

    int F;//!< yup i store F

    State* parent;//!< pointer toward the parent state on a path

    int x;//!< position of the state on the X axis
    int y;//!< position of the state on the Y axis

    bool inOpenList;//!< boolean to know if this state is in the OpenList

    bool inClosedList;//!< boolean to know if this state is in the ClosedList

    void debug()//!< ugly debug function
    {
        std::cout << "state " << x << " " << y <<std::endl;
        std::cout << "  H="<<H<< " D=" << D << " G="<< G <<  " F=" << F<<std::endl;
        std::cout << "  Open=" << inOpenList << " Closed=" << inClosedList<<std::endl;
    }
};

bool StateComp (State* i,State* j);//!< state comparison function, to keep the lists sorted

/** \brief AStar is the pathfinder class
 */
class AStar
{
    public:
        /** \brief Constructor of the class, must link to a map
         * \param m is a pointer toward a Map
         */
        AStar(Map* m);

        /** Default destructor */
        virtual ~AStar();

        /** \brief this function will calculate a path from the point located at sx,sy to the point ex,ey
         *
         * \param sx location of the startPoint on the x axis
         * \param sy location of the startPoint on the y axis
         * \param ex location of the end point on the x axis
         * \param ey location of the end point on the y axis
         */
        void calculatePath(int sx, int sy, int ex, int ey);

        State* getState(int i, int j);//!< return the state of the matrix at position i,j


        void addToOpen(int x, int y,State* p = NULL,int rawG = 0);

        void check8n(int x, int y);

        void swapToClosed();


    protected:

    Map* m_map;//!< pointer toward the map

    int m_size;

    int gx;
    int gy;

    std::deque<State*> m_openList;//!< A* open state list

    std::deque<State*> m_closedList;//!< A* closed state list

    std::vector<State*> m_matrix;//!< this is the state list that reflect the map

    const int DIRECT = 10;
    const int DIAG = 14;
};

#endif // ASTAR_H
