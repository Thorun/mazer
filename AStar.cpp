#include "AStar.h"

float heuristicFunction(int cx,int cy, int gx, int gy)
{
    return (abs(gx-cx)+abs(gy-cy))*10;

    //or you could use euclidian distance
    //return sqrt(((gx-cx)*(gx-cx))+((gy-cy)*(gy-cy)));
}

bool StateComp (State* i,State* j)
 { return (i->F<j->F); }


AStar::AStar(Map* m)
{
    m_map = m;
    m_size = m->Getsize();
    //mirror creation
    for(int i =0; i<m_map->Getsize(); i++)
    {
        for(int j =0; j<m_map->Getsize(); j++)
        {
            m_matrix.push_back(new State(j,i,m->getVal(j,i)));
        }
    }
    //ctor
}

State* AStar::getState(int i, int j)
{
    return m_matrix.at(i+(j*m_map->Getsize()));
}

AStar::~AStar()
{
    //dtor
    for(int i =0; i<m_matrix.size();i++)
    {
        delete m_matrix.at(i);
    }
    m_matrix.clear();
}
void AStar::calculatePath(int sx, int sy, int ex, int ey)
{
    gx = ex;
    gy = ey;
   addToOpen(sx, sy);//add the starting point to the opne list

    while(!getState(ex,ey)->inClosedList && !m_openList.empty())
    {
        //std::cout << "end : " << ex << "/" << ey<< " state :" << getState(ex,ey)->inClosedList<<std::endl;
        State* s = m_openList.front();
        swapToClosed();
        check8n(s->x,s->y);
/*
        for(int i =0; i<m_matrix.size(); i++)
        {
            m_matrix.at(i)->debug();
        }

        m_map->displayHR();
*/
    }

    //ohoho

    State* e = getState(ex,ey);
    while(e!= NULL)
    {
        m_map->setVal(e->x,e->y,2);
        e = e->parent;
    }
}

void AStar::check8n(int x, int y)
{
    State* p = getState(x,y);

    addToOpen(x-1,y-1,p,DIAG);
    addToOpen(x-1,y,p,DIRECT);
    addToOpen(x-1,y+1,p,DIAG);

    addToOpen(x,y-1,p,DIRECT);
    addToOpen(x,y+1,p,DIRECT);

    addToOpen(x+1,y-1,p,DIAG);
    addToOpen(x+1,y,p,DIRECT);
    addToOpen(x+1,y+1,p,DIAG);

    std::sort(m_openList.begin(),m_openList.end(),StateComp);
}

void AStar::addToOpen(int x, int y,State* p, int rawG)
{
    if(x >= 0 && x< m_size && y>= 0 && y < m_size)
    {
        State* s = getState(x,y);

        if(s->D != 1)
        if(!s->inClosedList)//we can't pass if it's impossible
        {
            if(!s->inOpenList)
            {
                s->parent = p;
                if(p!= NULL)
                s->G = p->G+rawG+s->D;//here we add the difficulty
                s->H = heuristicFunction(x,y,gx,gy);
                s->F = s->G+s->H;

                m_openList.push_back(s);
                m_map->setVal(s->x,s->y,4);
                s->inOpenList = true;
            }
            else if(p->G+rawG+s->D < s->G)//better path
            {
                s->parent = p;
                s->G = p->G+rawG+s->D;//here we add the difficulty
                s->F = s->G+s->H;
            }

        }
    }
}

void AStar::swapToClosed()
{
    State* s = m_openList.front();
    m_openList.pop_front();
    s->inOpenList = false;
    s->inClosedList = true;
    m_closedList.push_back(s);
    m_map->setVal(s->x,s->y,5);
}
