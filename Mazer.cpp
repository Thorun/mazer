#include "Mazer.h"

int random(int a, int b)
{
    return rand()%(b-a) +a;
}

//============================================================================================

Mazer::Mazer(Map* m):m_map(m)
{
    //ctor
}

//============================================================================================

Mazer::~Mazer()
{
    //dtor
}

//============================================================================================

void Mazer::Maze(coord TL,coord TR,coord BL, coord BR)
{
    int w = TR.x - TL.x;
    int h = BL.y - TL.y;

    if(w >=3 && h >=3 && (TR.x-2) > (TL.x+2) && ((BL.y-2)) > (TL.y+2))
    {
        if(w>=h)//we cut verticaly
        {
            int xcut = random(TL.x+2,TR.x-2);

            MakeWallVertical(xcut,TL.y+1, BL.y-1);
            Maze(TL,coord(xcut,TL.y),BL,coord(xcut,BL.y));
            Maze(coord(xcut,TL.y),TR,coord(xcut,BL.y),BR);
        }
        else
        {
            int ycut = random(TL.y+2,BL.y-2);

            MakeWallHorizontal(ycut,TL.x+1, TR.x-1);

            Maze(TL,TR,coord(TL.x,ycut),coord(TR.x,ycut));
            Maze(coord(TL.x,ycut),coord(TR.x,ycut),BL,BR);
        }
    }
}

//============================================================================================

void Mazer::MakeWallVertical(int x,int starty, int endy)
{
    for(int i = starty; i<=endy; i++)
    {
        m_map->setVal(x,i,1);
    }

    if(m_map->getVal(x,starty-1) == 0)
    {
        m_map->setVal(x,starty,0);
    }
    if(m_map->getVal(x,endy+1)==0)
    {
        m_map->setVal(x,endy,0);
    }
    int hole = random(starty,endy);
    m_map->setVal(x,hole,0);

}

//============================================================================================

void Mazer::MakeWallHorizontal(int y,int startx, int endx)
{
    for(int i = startx; i<=endx; i++)
    {
        m_map->setVal(i,y,1);
    }

    if(m_map->getVal(startx-1,y) == 0)
    {
        m_map->setVal(startx,y,0);
    }
    if(m_map->getVal(endx+1,y)==0)
    {
        m_map->setVal(endx,y,0);
    }


    int hole = random(startx,endx);
    m_map->setVal(hole,y,0);

}
