#include "Map.h"

Map::Map(int size):m_size(size)
{
    for(int i =0; i<size*size; i++)
    {
        m_matrix.push_back(0);
    }
    //ctor
}

//============================================================================================

Map::~Map()
{
    //dtor
}

//============================================================================================

void Map::randomap()
{
    for(int i =0; i<m_size; i++)
    {
        for(int j=0; j<m_size; j++)
        {
            setVal(j,i,coinFlip());
        }
    }
    //ctor
}

//============================================================================================

void Map::border()
{
    for(int i=0;i<m_size;i++)
    {
        setVal(i,0,1);
        setVal(i,m_size-1,1);
        setVal(0,i,1);
        setVal(m_size-1,i,1);
    }
}

//============================================================================================

bool Map::coinFlip()
{
    int r = rand() % 100;

    if(r<50)
    {
        return false;
    }
    return true;
}

//============================================================================================

void Map::display()
{
    for(int i =0; i<m_size; i++)
    {
        for(int j=0; j<m_size; j++)
        {
            std::cout << getVal(j,i);
        }
        std::cout << std::endl;
    }
}

//============================================================================================

void Map::displayHR()
{
        for(int i =0; i<m_size; i++)
    {
        for(int j=0; j<m_size; j++)
        {
            int v= getVal(j,i);
            switch(v)
            {
                case 0:
                std::cout << " ";
                break;
                case 1:
                std::cout << "#";
                break;
                case 2:
                std::cout << "X";
                break;
                case 3:
                std::cout << "0";
                break;
                case 4:
                std::cout << "?";
                break;
                case 5:
                std::cout << " ";
                break;
                default:
                std::cout << " ";
                std::cout << std::endl;
                break;

            }
        }
        std::cout << std::endl;
    }
}
